<?php


namespace App\Connex;


abstract class ConnexModel
{
    private static $db_host = '172.23.0.1';
    private static $db_user = 'user';
    private static $db_pass = 'test';//password
    protected $db_name = '';//test
    private static $db_charset = 'utf8';
    private $conn;
    protected $query;
    protected $rows = array();

    //Métodos abstractos para CRUD de clases que heradan

    abstract protected function create();

    abstract protected function read();

    abstract protected function update();

    abstract protected function delete();

    // método privado para conectarse a BBDD
    private function db_open()
    {
        //http://php.net/manual/es/class.mysqli.php
        $this->conn = new mysqli(
            self::$db_host,
            self::$db_user,
            self::$db_pass,
            $this->db_name
        );
        //https://www.php.net/manual/es/mysqli.set-charset.php
        $this->conn->set_charset(self::$db_charset);
    }

    // método privado para desconectarse a BBDD
    private function db_close()
    {
        https://www.php.net/manual/es/mysqli.close.php
        $this->conn->close();
    }

    /**
     *Establecer un query que afecte datos (INSERT, DELETE o UPDATE)
     */
    protected function set_query()
    {
        $this->db_open();
        //https://www.php.net/manual/es/mysqli.query.php
        $this->conn->query($this->query);
        $this->db_close();
    }


    /**
     * Obtener datos de un query (SELECT)
     * @return array
     */
    protected function get_query()
    {
        $this->db_open();

        $result = $this->conn->query($this->query);
        //https://www.php.net/manual/es/mysqli-result.fetch-assoc.php
        //https://www.php.net/manual/es/mysqli-result.fetch-row.php
        while ($this->rows[] = $result->fetch_assoc()) ;
        $result->close();

        $this->db_close();
        //https://www.php.net/manual/es/function.array-pop.php
        return array_pop($this->rows);
    }
}
