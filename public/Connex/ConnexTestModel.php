<?php
namespace App\Connex;

include_once ('./Connex/ConnexModel.php');

use App\Connex\ConnexModel;

class ConnexTestModel extends ConnexModel
{

    protected function create()
    {
        // TODO: Implement create() method.
    }

    protected function read( $id = '')
    {
        $this->query = ($id !='')
            ? "SELECT * FROM  $this->db_name WHERE id = $id"
            : "SELECT * FROM $this->db_name";

        $this->get_query();
        return $this->rows;
    }

    protected function update()
    {
        // TODO: Implement update() method.
    }

    protected function delete()
    {
        // TODO: Implement delete() method.
    }

    public function __construct()
    {
        $this->db_name='test';
    }

/*    public function __unset()
    {
       unset($this);
    }*/
}
